# Apollo

- While using apollo the nessessary pakages are:

    - apollo-boost
    - react-apollo
    - graphql (graphql-tag)

```
    import ApolloClient from 'apollo-boost';
    import {ApolloProvider} from 'react-apollo';
```
- In index.js create a new client using `ApolloClient` for the graphql endpoint

```
    const client = new ApolloClient({
        uri: <url>
    });
```

` Then wrap the **App** component with the `ApolloProvider`

```
    <ApolloProvider>
        <App />
    </ApolloProvider>
```

# react-router-dom

- In index.js import `BrowserRouter` and `Route` 

```
    import {BrowserRouter as Router, Route} from 'react-router-dom'
```

- Wrap all the `routes` within the `Router`

```
    <Router>
        <Route path="/" component={Home}/>
        <Route path="/about" component={about}/>
    </Router>
```

- Use `history.push()` method to route between pages